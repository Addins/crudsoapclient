package com.app.viewmodels;

import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import com.app.dao.DaoCategoryImpl;
import com.app.dao.ICrudSimpleDao;
import com.app.domain.BookDTO;
import com.app.domain.CategoryDTO;

public class NewBookVM {
	
//	private ICrudSimpleDao<BookDTO> daoBook;
	private ICrudSimpleDao<CategoryDTO> daoCategory;
	
	private BookDTO newBook;
	private List<CategoryDTO> listCategory;
	
	@Init
	public void init(){
//		daoBook = new DaoBookImpl();
		Map<String, Object> map = (Map<String, Object>) Executions.getCurrent().getArg();
		Clients.showNotification((String) map.get("data"));
		daoCategory = new DaoCategoryImpl();
		newBook = new BookDTO();
		getDataFromDao();
	}
	
	private void getDataFromDao(){
		setListCategory(daoCategory.getAll());
	}

	public List<CategoryDTO> getListCategory() {
		return listCategory;
	}

	public void setListCategory(List<CategoryDTO> listCategory) {
		this.listCategory = listCategory;
	}

	public BookDTO getNewBook() {
		return newBook;
	}

	public void setNewBook(BookDTO newBook) {
		this.newBook = newBook;
	}
	
	@Command
	public void save(@SelectorParam("#winNewBook")Window w){
		w.detach();
		EventQueues.lookup("NewBookBro").publish(new Event("onNewBook", null, newBook));
	}
	
}

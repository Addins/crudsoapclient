package com.app.viewmodels;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import com.app.dao.DaoCategoryImpl;
import com.app.dao.ICrudSimpleDao;
import com.app.domain.CategoryDTO;

public class CategoryListVM {

	private ICrudSimpleDao<CategoryDTO> daoCategory;

	private List<CategoryDTO> categoryList;

	private CategoryDTO selected;

	/*
	 * "false" = wform invisible "true" = wform visible
	 */
	private boolean wFormVisible = false;

	private String dtFormat = "yyyy/MM/dd h:ma";

	@Init
	public void init() {
		wFormVisible = false;
		daoCategory = new DaoCategoryImpl();
		getDataFromDao();
	}

	private void getDataFromDao() {
		categoryList = new ArrayList(daoCategory.getAll());
		selected = null;
	}

	public void setSelected(CategoryDTO selectedCategory) {
		this.selected = selectedCategory;
	}

	public CategoryDTO getSelected() {
		return selected;
	}

	public List<CategoryDTO> getCategoryList() {
		return categoryList;
	}

	public boolean iswFormVisible() {
		return wFormVisible;
	}

	@NotifyChange(".")
	public void setwFormVisible(boolean wFormVisible) {
		this.wFormVisible = wFormVisible;
	}

	public String getDtFormat() {
		return dtFormat;
	}

	@NotifyChange("categoryList")
	@Command
	public void refresh() {
		getDataFromDao();
	}

	@NotifyChange(".")
	@Command
	public void addCategory() {
		selected = new CategoryDTO();
		wFormVisible = true;
	}

	@NotifyChange(".")
	@Command
	public void editCategory() {
		if (null != selected) {
			wFormVisible = true;
		}
	}

	@NotifyChange(".")
	@Command
	public void deleteCategory() {
		if (null != selected) {
			final Object obj = this;
			Messagebox.show("Are you sure?", "Confirm to delete selected item",
					Messagebox.YES + Messagebox.NO, Messagebox.QUESTION,
					new EventListener<Event>() {
						@Override
						public void onEvent(Event event) throws Exception {
							switch (((Integer) event.getData()).intValue()) {
							case Messagebox.YES:
								delete();
								BindUtils.postNotifyChange(null, null, obj,
										".");
								break;
							}
						}
					});
		}
	}
	
	private void delete() {
		try {
			CategoryDTO d = daoCategory.delete(selected.getId());
			refresh();
			Clients.showNotification("Deleted Category: \n" + d.getName());
		} catch (Exception e) {
			Clients.showNotification(e.getMessage(), "error", null, "", 0);
			e.printStackTrace();
		}
	}

	@NotifyChange(".")
	@Command
	public void saveCategory() {
		CategoryDTO c = null;
		try {
			c = daoCategory.save(selected);
			Clients.showNotification("Category \"" + c.getName() + "\" saved.");
		} catch (Exception e) {
			Clients.showNotification("Unable to save Updated category.",
					"error", null, "top_center", 0);
			e.printStackTrace();
		}
		refresh();
		wFormVisible = false;
	}

	@NotifyChange(".")
	@Command
	public void closeWForm() {
		selected = null;
		wFormVisible = false;
	}

}

package com.app.viewmodels;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Form;
import org.zkoss.bind.SimpleForm;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.SelectorParam;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.app.dao.DaoAuthorImpl;
import com.app.dao.ICrudSimpleDao;
import com.app.domain.AuthorDTO;
import com.app.domain.BookDTO;

public class AuthorListVM {
	
	static Form meForm = new SimpleForm();

	private ICrudSimpleDao<AuthorDTO> daoAuthor;

	private List<AuthorDTO> authorList;

	private AuthorDTO selected;
	private AuthorDTO formData = new AuthorDTO();

	/*
	 * "false" = wform invisible "true" = wform visible
	 */
	private boolean wFormVisible = false;

	private String dtFormat = "yyyy/MM/dd";

	@Init
	public void init() {
		daoAuthor = new DaoAuthorImpl();
		getDataFromDao();
	}

	private void getDataFromDao() {
		authorList = daoAuthor.getAll();
		selected = null;
	}
	
	public Form getMeForm() {
		return meForm;
	}

	@NotifyChange(".")
	public void setwFormVisible(boolean wFormVisible) {
		this.wFormVisible = wFormVisible;
	}

	public boolean iswFormVisible() {
		return wFormVisible;
	}

	public String getDtFormat() {
		return dtFormat;
	}

	public void setSelected(AuthorDTO selected) {
		this.selected = selected;
	}

	public AuthorDTO getSelected() {
		return selected;
	}

	public List<AuthorDTO> getAuthorList() {
		return authorList;
	}

	private void delete() {
		try {
			AuthorDTO a = daoAuthor.delete(selected.getId());
			refresh();
			Clients.showNotification("Deleted Author: \n" + a.getName());
		} catch (Exception e) {
			Clients.showNotification(e.getMessage(), "error", null, "", 0);
			e.printStackTrace();
		}
	}

	@NotifyChange(".")
	@Command
	public void addBook(@SelectorParam("#wDetail")Window win) {
		formData.setId((Long) meForm.getField("id"));
		formData.setName((String) meForm.getField("name"));
		Window window = (Window) Executions.createComponents(
				"pages/new_book_form.zul", win.getParent(), null);
		window.doModal();
		final Object obj = this;
		EventQueues.lookup("NewBookBro").subscribe(new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				if (event.getName().equals("onNewBook")) {
					BookDTO b = (BookDTO) event.getData();
					Set<BookDTO>booklist = selected.getBooks();
					if(booklist==null)booklist = new HashSet();
					booklist.add(b);
					selected.setBooks(booklist);
					BindUtils.postNotifyChange(null, null, obj, ".");
				}
			}
		});
	}

	@Command
	public void removeBook() {
		Clients.alert("rem");
	}

	@NotifyChange(".")
	@Command
	public void authorDetail() {
		wFormVisible = true;
	}

	@NotifyChange("authorList")
	@Command
	public void refresh() {
		getDataFromDao();
	}

	@NotifyChange(".")
	@Command
	public void addAuthor() {
		selected = new AuthorDTO();
		wFormVisible = true;
	}

	@NotifyChange(".")
	@Command
	public void editAuthor() {
		if (null != selected) {
			wFormVisible = true;
		}
	}

	@NotifyChange(".")
	@Command
	public void deleteAuthor() {
		if (null != selected) {
			final Object obj = this;
			Messagebox.show("Are you sure?", "Confirm to delete this author",
					Messagebox.YES + Messagebox.NO, Messagebox.QUESTION,
					new EventListener<Event>() {
						@Override
						public void onEvent(Event event) throws Exception {
							switch (((Integer) event.getData()).intValue()) {
							case Messagebox.YES:
								delete();
								wFormVisible = false;
								BindUtils
										.postNotifyChange(null, null, obj, ".");
								break;
							}
						}
					});
		}
	}

	@NotifyChange(".")
	@Command
	public void save() {
		AuthorDTO c = null;
		try {
			c = daoAuthor.save(selected);
			Clients.showNotification("Author \"" + c.getName() + "\" saved.");
		} catch (Exception e) {
			Clients.showNotification("Unable to save Updated author.", "error",
					null, "top_center", 0);
			e.printStackTrace();
		}
		refresh();
		wFormVisible = false;
	}

	@NotifyChange(".")
	@Command
	public void closeWForm() {
		selected = null;
		wFormVisible = false;
	}
}

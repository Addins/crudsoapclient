package com.app.dao;

import java.util.List;

public interface ICrudSimpleDao<T> {
	public T save(T item) throws Exception;
	public T delete(long id) throws Exception;
	public List<T> getAll();
	public T get(long id);
}

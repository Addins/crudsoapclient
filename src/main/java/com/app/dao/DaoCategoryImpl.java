package com.app.dao;

import java.util.List;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import com.app.domain.CategoryDTO;
import com.app.services.CategoryService;

public class DaoCategoryImpl implements ICrudSimpleDao<CategoryDTO> {

	private CategoryService service;
	private String serviceUrl = "http://localhost:8080/CrudSoapServer/services/category";
	private JaxWsProxyFactoryBean wsProxyFactoryBean;

	public DaoCategoryImpl() {
		reinitialize();
	}
	
	private void reinitialize(){
		wsProxyFactoryBean = new JaxWsProxyFactoryBean();
		wsProxyFactoryBean.setServiceClass(CategoryService.class);
		wsProxyFactoryBean.setAddress(serviceUrl);
		service = (CategoryService) wsProxyFactoryBean.create();
	}

	@Override
	public CategoryDTO save(CategoryDTO item) throws Exception {
		return service.save(item);
	}

	@Override
	public CategoryDTO delete(long id) throws Exception {
		return service.delete(id);
	}

	@Override
	public List<CategoryDTO> getAll() {
		return service.getAll();
	}

	@Override
	public CategoryDTO get(long id) {
		return service.get(id);
	}

}

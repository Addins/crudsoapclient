package com.app.dao;

import java.util.List;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import com.app.domain.BookDTO;
import com.app.services.BookService;

public class DaoBookImpl implements ICrudSimpleDao<BookDTO> {

	BookService service;
	private String serviceUrl = "http://localhost:8080/CrudSoapServer/services/book";
	private JaxWsProxyFactoryBean wsProxyFactoryBean;
	
	public DaoBookImpl() {
		wsProxyFactoryBean = new JaxWsProxyFactoryBean();
		wsProxyFactoryBean.setServiceClass(BookService.class);
		wsProxyFactoryBean.setAddress(serviceUrl);
		service = (BookService) wsProxyFactoryBean.create();
	}
	
	@Override
	public BookDTO save(BookDTO item) throws Exception {
		return service.save(item);
	}

	@Override
	public BookDTO delete(long id) throws Exception {
		return service.delete(id);
	}

	@Override
	public List<BookDTO> getAll() {
		return service.getAll();
	}

	@Override
	public BookDTO get(long id) {
		return service.get(id);
	}

}

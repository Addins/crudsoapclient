package com.app.dao;

import java.util.List;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import com.app.domain.AuthorDTO;
import com.app.services.AuthorService;

public class DaoAuthorImpl implements ICrudSimpleDao<AuthorDTO> {

	AuthorService service;
	private String serviceUrl = "http://localhost:8080/CrudSoapServer/services/author";
	private JaxWsProxyFactoryBean wsProxyFactoryBean;
	
	public DaoAuthorImpl() {
		reinitialize();
	}
	
	private void reinitialize(){
		wsProxyFactoryBean = new JaxWsProxyFactoryBean();
		wsProxyFactoryBean.setServiceClass(AuthorService.class);
		wsProxyFactoryBean.setAddress(serviceUrl);
		service = (AuthorService) wsProxyFactoryBean.create();
	}
	
	@Override
	public AuthorDTO save(AuthorDTO item) throws Exception {
		return service.save(item);
	}

	@Override
	public AuthorDTO delete(long id) throws Exception {
		return service.delete(id);
	}

	@Override
	public List<AuthorDTO> getAll() {
		return service.getAll();
	}

	@Override
	public AuthorDTO get(long id) {
		return service.get(id);
	}
}

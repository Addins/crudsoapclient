package com.app.services;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.app.domain.BookDTO;

@WebService
public interface BookService {
	@WebMethod
	public BookDTO save(BookDTO item) throws Exception;
	@WebMethod
	public BookDTO delete(long id) throws Exception;
	@WebMethod
	public List<BookDTO> getAll();
	@WebMethod
	public BookDTO get(long id);
}

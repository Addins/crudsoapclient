package com.app.services;

import java.util.List;

import com.app.domain.CategoryDTO;
import javax.jws.WebService;
import javax.jws.WebMethod;

@WebService
public interface CategoryService {
	@WebMethod
	public CategoryDTO save(CategoryDTO item) throws Exception;
	@WebMethod
	public CategoryDTO delete(long id) throws Exception;
	@WebMethod
	public List<CategoryDTO> getAll();
	@WebMethod
	public CategoryDTO get(long id);
}

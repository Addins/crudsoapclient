package com.app.services;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.app.domain.AuthorDTO;

@WebService
public interface AuthorService {
	@WebMethod
	public AuthorDTO save(AuthorDTO item) throws Exception;
	@WebMethod
	public AuthorDTO delete(long id) throws Exception;
	@WebMethod
	public List<AuthorDTO> getAll();
	@WebMethod
	public AuthorDTO get(long id);
}

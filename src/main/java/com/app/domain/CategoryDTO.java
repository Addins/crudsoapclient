package com.app.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.eclipse.persistence.oxm.annotations.XmlInverseReference;
@XmlAccessorType(XmlAccessType.NONE)
public class CategoryDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @XmlElement
    private Long id;
    @XmlInverseReference(mappedBy="category")
    private Set<BookDTO> books;
    @XmlElement
    private Date created;
    @XmlElement
    private String name;

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }
    public Set<BookDTO> getBooks() { return books; }
    public void setBooks(Set<BookDTO> books) { this.books = books; }
    public Date getCreated() { return created; }
    public void setCreated(Date created) { this.created = created; }
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CategoryDTO[");
        sb.append("\n\tid: " + this.id);
//        sb.append("\n\tbooks: " + this.books);
        sb.append("\n\tcreated: " + this.created);
        sb.append("\n\tname: " + this.name);
        sb.append("]");
        return sb.toString();
    }
}

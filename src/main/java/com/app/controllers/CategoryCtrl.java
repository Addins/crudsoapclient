package com.app.controllers;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Window;

import com.app.dao.DaoCategoryImpl;
import com.app.dao.ICrudSimpleDao;
import com.app.domain.CategoryDTO;

public class CategoryCtrl extends SelectorComposer<Component> {
	private static final long serialVersionUID = -8714865902340789376L;

	// @WireVariable
	ICrudSimpleDao<CategoryDTO> daoCategory;

	@Wire
	private Window win;
	@Wire
	private Listbox lsbox_category;

	private ListModel<CategoryDTO> categoryModel;

	public CategoryCtrl() {
		daoCategory = new DaoCategoryImpl();
		retrieveCategoryToListModel();
		Calendar c = Calendar.getInstance();
	}

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		refresh();
	}

	private void retrieveCategoryToListModel() {
		try {
			categoryModel = new ListModelList<CategoryDTO>(daoCategory.getAll());
		} catch (Exception x) {
		}
	}

	public ListModel<CategoryDTO> getCategoryModel() {
		return categoryModel;
	}

	@Listen("onClick=#btn_lsbox_refresh")
	public void refresh() {
		retrieveCategoryToListModel();
		lsbox_category.setModel(categoryModel);
	}

	@Listen("onClick=#btn_add")
	public void addCategory() {
		Window formWindow = (Window) Executions.createComponents(
				"/pages/form_category.zul", win, null);
		formWindow.doModal();
		EventQueues.lookup("BagianCategory").subscribe(
				new EventListener<Event>() {
					@Override
					public void onEvent(Event event) throws Exception {
						if (event.getName().equals("onCategorySaved")) {
							refresh();
						}
					}
				});
	}

	@Listen("onClick=#btn_edit")
	public void editCategory(Event ev) {
		int selectedIdx = lsbox_category.getSelectedIndex();
		CategoryDTO selectedCat = (CategoryDTO) lsbox_category.getModel()
				.getElementAt(selectedIdx);
		Map<String, Object> data = new HashMap();
		data.put("data", selectedCat);
		Window formWindow = (Window) Executions.createComponents(
				"/pages/form_category.zul", win, data);
		formWindow.doModal();
		EventQueues.lookup("BagianCategory").subscribe(
				new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						if (event.getName().equals("onCategorySaved")) {
							refresh();
						}
					}
				});
	}

	@Listen("onClick=#btn_delete")
	public void deleteCategory(Event ev) {
		int selectedIdx = lsbox_category.getSelectedIndex();
		CategoryDTO selectedCat = (CategoryDTO) lsbox_category.getModel()
				.getElementAt(selectedIdx);
		CategoryDTO c = null;
		try {
			c = daoCategory.delete(selectedCat.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (null == c) {
			Clients.showNotification("Unable to delete category.", "error",
					null, "top_center", 0);
		} else {
			Clients.showNotification("Category deleted.");
		}
		refresh();
	}
}

package com.app.controllers;

import java.util.List;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.app.dao.DaoCategoryImpl;
import com.app.dao.ICrudSimpleDao;
import com.app.domain.CategoryDTO;

public class CategoryFormCtrl extends SelectorComposer<Component> {
	private static final long serialVersionUID = -640096793685273413L;

	ICrudSimpleDao<CategoryDTO> daoCategory;
	Map<String, Object> map;
	CategoryDTO editedDto = null;

	@Wire
	Window wForm;
	@Wire
	Textbox txtbx_name;
	@Wire
	Textbox txtbx_id;
	@Wire
	Combobox cbb_cobap;

	public CategoryFormCtrl() {
		daoCategory = new DaoCategoryImpl();
	}

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);

		List<CategoryDTO> listC = daoCategory.getAll();
		cbb_cobap.setModel(new ListModelList<CategoryDTO>(listC));
		map = (Map<String, Object>) Executions.getCurrent().getArg();
		editedDto = (CategoryDTO) map.get("data");
		if (editedDto != null)
			txtbx_name.setText(editedDto.getName());
	}

	@Listen("onClick=#btn_modal")
	public void modal() {
		Clients.alert("Tes Aja");
	}

	@Listen("onClick=#btn_save")
	public void saveCategory() throws Exception {

		String name = txtbx_name.getText();
		if (editedDto == null)
			editedDto = new CategoryDTO();
		editedDto.setName(name);
		CategoryDTO c = daoCategory.save(editedDto);
		if (null == c) {
			Clients.showNotification("Unable to save Updated category.",
					"error", null, "top_center", 0);
		} else {
			Clients.showNotification("Category \"" + c.getName() + "\" saved.");
		}
		EventQueues.lookup("BagianCategory").publish(
				new Event("onCategorySaved"));
		wForm.detach();
	}

	@Listen("onClick=#btn_cancel")
	public void cancel() {
//		long selected = cbb_cobap.getSelectedItem().getValue();
//		Clients.showNotification(selected+"");
		for (Comboitem it : cbb_cobap.getItems()) {
			if(it.getValue().equals(5L)){
				cbb_cobap.setSelectedItem(it);
				break;
			}
		}
//		wForm.detach();
	}
}
